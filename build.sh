#!/bin/bash -x

if [ x$1 = "x" ];then
   #REFNAME=myrelease
   REFNAME=kinoite
else
   REFNAME=$1
fi

BASE=/
REPO_BASE=/var/ostreerepo
CACHEDIR=${REPO_BASE}/cache
BUILDREPODIR=${REPO_BASE}/build-repo
DEPLOYREPODIR=${REPO_BASE}/deploy-repo
OSTREE_CONFIG_DIR=${BASE}/workstation-ostree-config
OSTREE_CONFIG_FILE=${OSTREE_CONFIG_DIR}/fedora-${REFNAME}.yaml
UPDATE_REPO_REF=fedora/rawhide/x86_64/kinoite
REMOVE_YAML_LIST="fedora-common-ostree-pkgs.yaml
fedora-kinoite.yaml
kde-desktop-pkgs.yaml"
REMOVE_LIST="removelist"
ADD_LIST="addlist"

git -C ${OSTREE_CONFIG_DIR} pull --ff-only
cat /${ADD_LIST} >> ${OSTREE_CONFIG_DIR}/kde-desktop-pkgs.yaml

for YAML in ${REMOVE_YAML_LIST}
do
    for PKG in $(cat ${REMOVE_LIST})
    do
        sed -i -e "/^.*${PKG}/d" ${OSTREE_CONFIG_DIR}/${YAML}
    done
done

for DIR in ${BUILDREPODIR} ${CACHEDIR} ${DEPLOYREPODIR}
do
    if [ ! -d ${DIR} ];then
        mkdir -p ${DIR}
    fi 
done

ostree init --repo=${BUILDREPODIR} --mode=bare-user

if [ ! -e ${DEPLOYREPODIR}/summary ]; then
    ostree init --mode=archive --repo=${DEPLOYREPODIR}
fi

rpm-ostree compose tree --unified-core --cachedir=${CACHEDIR} --repo=${BUILDREPODIR} ${OSTREE_CONFIG_FILE}

ostree --repo=${DEPLOYREPODIR} pull-local ${BUILDREPODIR} ${UPDATE_REPO_REF}

#if [ $? -eq 0 ]; then
#    ostree summary --repo=${DEPLOYREPODIR} -u
#fi

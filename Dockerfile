FROM registry.fedoraproject.org/fedora-minimal:34
MAINTAINER Takahiro Hashimoto

ENV SCRIPT build.sh
ENV ADDFILE addlist
ENV REMOVEFILE removelist
ENV REFNAME kinoite
ENV BRANCH main
ENV REPO=https://pagure.io/workstation-ostree-config.git

WORKDIR /
VOLUME ["/var/ostreerepo"]
COPY $SCRIPT /$SCRIPT
COPY files/$ADDFILE /$ADDFILE
COPY files/$REMOVEFILE /$REMOVEFILE
RUN chmod 555 /$SCRIPT \
 && microdnf update -y \
 && microdnf install -y rpm-ostree git-core \
 && git clone $REPO --branch $BRANCH \
 && microdnf clean all

ENTRYPOINT ["/bin/bash"]
CMD ["-c", "/build.sh", "$REFNAME"]
